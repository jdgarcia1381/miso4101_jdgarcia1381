# coding=utf-8
__author__ = 'Juan David García Manrique'


# Función de Fibonacci (recursiva)
def fibonacci(n):
    if n <= 1:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


# Input del usuario
iteraciones = int(input("Ingrese el número de iteraciones:"))

# Valida el número ingresado
while iteraciones <= 0:
    print("Debe ingresar un número de iteraciones válido")
    iteraciones = int(input("Ingrese el número de iteraciones:"))

sequencia = ''
for i in range(iteraciones):
    coma = ', '
    if i == 0:
        coma = ''
    sequencia += (coma + str(fibonacci(i)))
print "Sequencia Fibonacci: " + sequencia
